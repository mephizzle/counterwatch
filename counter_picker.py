from collections import defaultdict, OrderedDict

from counter_list import COUNTER_DATA

first = raw_input("Enter the first hero: ")
second = raw_input("Enter the second hero: ")
third = raw_input("Enter the third hero: ")
fourth = raw_input("Enter the fourth hero: ")
fifth = raw_input("Enter the fifth hero: ")
sixth = raw_input("Enter the sixth hero: ")

enemy_heroes = [first, second, third, fourth, fifth, sixth]

counter_heroes = []

for hero in enemy_heroes:
    if hero in COUNTER_DATA:
        counters = COUNTER_DATA[hero]
        counter_heroes.append(counters)

final_counters = defaultdict(int)

for x in counter_heroes:
    for y in x:
        final_counters[y] += 1

counters_ordered = OrderedDict(sorted(
    final_counters.items(),
    key=lambda t: t[1],
    reverse=True))

print counters_ordered.keys()[:6]
