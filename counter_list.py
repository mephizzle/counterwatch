
COUNTER_DATA = {
    # tanks
    'd.va': ['hanzo', 'soldier76', 'zarya', 'zenyatta', 'symmetra', 'lucio'],
    'reinhardt': ['mei', 'junkrat', 'bastion', 'tracer', 'symmetra', 'reaper'],
    'roadhog': ['mercy', 'torbjorn', 'soldier76', 'reinhardt', 'zenyatta', 'tracer', 'reaper', 'mccree', 'hanzo'],
    'winston': ['lucio', 'mei', 'bastion', 'zenyatta', 'reaper', 'mccree'],
    'zarya': ['zenyatta', 'reinhardt', 'pharah'],

    # assault
    'tracer': ['winston', 'lucio', 'torbjorn', 'pharah', 'junkrat'],
    'mccree': ['zarya'],
    'pharah': ['pharah', 'roadhog', 'd.va', 'zenyatta', 'widowmaker', 'mccree', 'soldier76'],
    'reaper': ['mccree', 'pharah'],
    'soldier76': ['junkrat'],
    'genji': ['symmetra', 'torbjorn', 'mei', 'zarya', 'winston', 'pharah'],

    # defense
    'bastion': ['junkrat', 'hanzo', 'reaper', 'd.va', 'roadhog', 'widowmaker', 'pharah', 'tracer', 'genji'],
    'hanzo': ['reinhardt', 'tracer'],
    'junkrat': ['roadhog', 'pharah', 'mccree', 'lucio'],
    'mei': ['mercy', 'lucio', 'reaper', 'pharah', 'widowmaker', 'mccree'],
    'torbjorn': ['mercy', 'mei', 'pharah', 'reinhardt', 'widowmaker', 'd.va', 'junkrat', 'zarya'],
    'widowmaker': ['d.va', 'reinhardt', 'winston', 'tracer', 'genji'],

    # support
    'lucio': ['roadhog', 'genji', 'mccree', 'widowmaker'],
    'mercy': ['hanzo', 'tracer', 'widowmaker', 'mccree', 'genji'],
    'symmetra': ['winston', 'junkrat', 'zarya'],
    'zenyatta': ['junkrat', 'widowmaker', 'genji', 'tracer', 'reinhardt']
}
